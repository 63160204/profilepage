import 'package:flutter/material.dart';
enum APP_THEME{LIGHT, DARK}

void main() {
  runApp(ContactProfilePage());
}
class MyAppTheme{
  static ThemeData appThemeLight(){
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
          color: Colors.white,
          iconTheme:  IconThemeData(
            color: Colors.black,
          ),
        ) ,
        iconTheme: IconThemeData(
          color: Colors.indigo.shade500,
        ),
    );
  }


static ThemeData appThemeDark()
{
return ThemeData(
  brightness: Brightness.dark,
  appBarTheme: AppBarTheme(
    color: Colors.white,
    iconTheme:  IconThemeData(
      color: Colors.black,
    ),
  ) ,
  iconTheme: IconThemeData(
    color: Colors.indigo.shade500,
  )
);

}
}
class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
          ? MyAppTheme.appThemeLight()
        :MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.threesixty),
          onPressed: () {
            setState(() {
              currentTheme == APP_THEME.DARK
                  ? currentTheme = APP_THEME.LIGHT
                  : currentTheme = APP_THEME.DARK;
            });
          },
        ),
      ),
    );
  }
}
Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
         // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
         // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
        //  color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
         // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
       //   color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}

//ListTile
Widget mobilePhoneListTile() {
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("092-8762741"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      //color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

Widget otherPhoneListTile() {
  return ListTile(
    leading: Text(""),
    title: Text("330-803-3390"),
    subtitle: Text("other"),
    trailing: IconButton(
      icon: Icon(Icons.message),
    //  color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

Widget emailListTile() {
  return ListTile(
    leading: Icon(Icons.email),
    title: Text("63160204@go.buu.ac.th"),
    subtitle: Text("work"),
  );
}

Widget addressListTile() {
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text("79 Pharchinburi"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.directions),
      //color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

AppBar buildAppBarWidget() {
  return AppBar(
  backgroundColor: Colors.pink[100],
  leading: Icon(
  Icons.arrow_back,
  color: Colors.white,
  ),
  actions: <Widget>[
  IconButton(
  onPressed: () {},
  icon: Icon(Icons.star_border),
  color: Colors.white,
  )
  ],
  );
  }

  Widget buildBodyWidget() {
    return ListView(
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              width: double.infinity,

              //Height constraint at Container widget level
              height: 250,

              child: Image.network(
                "https://scontent.fbkk10-1.fna.fbcdn.net/v/t1.6435-9/151777122_1377840489219161_197593444705939836_n.jpg?_nc_cat=102&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeGHJj3jMP3_CDjkVnqSxKpzOagCN-Cb6es5qAI34Jvp6-OxoXsna3RbdtLSssojO7jw9pfFAPb3guoGMx5CRssJ&_nc_ohc=t-cI7QmNLdwAX9on57S&tn=VhtPJD1QDlPTDbB8&_nc_ht=scontent.fbkk10-1.fna&oh=00_AfABFfIjISI-ARnunx9M1Y4Vz799tjNIjbKyVcp_UIOWaQ&oe=63CA036B",

                fit: BoxFit.cover,
              ),
            ),
            Container(
              height: 60,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      "Patthamawan Lamphueng",
                      style: TextStyle(fontSize: 30),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 8, bottom: 8),
              child: Theme(
                data: ThemeData(
                  iconTheme: IconThemeData(
                    color: Colors.pinkAccent,
                  ),
                ),
                child: profileActionItems(),
              ),
            ),
            mobilePhoneListTile(),
            otherPhoneListTile(),
            emailListTile(),
            addressListTile(),
          ],
        ),
      ],
    );
  }

  Widget profileActionItems(){
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionsButton(),
      buildPayButton(),
    ],
  );
  }